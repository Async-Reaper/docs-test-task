import React, { FC } from "react";
import { IModalWindow } from "./ModalWindow.types";
import cl from "./ModalWindow.module.css";

/**
 * Component for showing modal window.
 *
 * @component
 * @example
 * const [visible, setVisible] = useState<boolean>(false)
 * return (
 *   <ModalWindow visible={visible} setVisible={setVisible}>
 *    <div>
 *      Child components
 *    </div>
 *  </ModalWindow>
 * )
 */

const ModalWindow: FC<IModalWindow> = ({ visible, setVisible, children }) => {
  const rootClasses = [cl.myModal];

  if (visible) {
    rootClasses.push(cl.active);
  }
  return (
    <div className={rootClasses.join(" ")} onClick={() => setVisible(false)}>
      <div className={cl.myModalContent} onClick={(e) => e.stopPropagation()}>
        {children}
      </div>
    </div>
  );
};

export default ModalWindow;
