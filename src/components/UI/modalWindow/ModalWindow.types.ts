import { ReactNode } from "react";

export interface IModalWindow {
  visible: boolean;
  setVisible: (arg: boolean) => void;
  children: ReactNode;
}
