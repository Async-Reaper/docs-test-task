import { ReactNode } from "react";

export interface IError {
  children: ReactNode;
}
