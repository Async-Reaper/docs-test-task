import React, { FC } from "react";
import { IError } from "./Error.types";
import cl from "./Error.module.css";

/**
 * Component for show text error.
 *
 * @component
 * @example
 * return (
 *   <Error>Error text</Error>
 * )
 */

const Error: FC<IError> = ({ children }) => {
  return <p className={cl.Error}>{children}</p>;
};

export default Error;
