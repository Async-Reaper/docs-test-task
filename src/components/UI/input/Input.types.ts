import React from "react";

export interface IInput {
  id: string;
  type: string;
  placeholder?: string;
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
  min?: string;
}
