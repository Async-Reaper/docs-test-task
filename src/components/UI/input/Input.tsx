import React, { FC } from "react";
import { IInput } from "./Input.types";
import cl from "./Input.module.css";

/**
 * Component for showing input.
 *
 * @component
 * @example
 * return (
 *   <Input type="text" placeholder="login" value={123} onChange={(e) => setState(e.targer.value)}/>
 * )
 */

export const Input: FC<IInput> = ({ type, placeholder, value, onChange, min }) => {
  return <input className={cl.Input} type={type} placeholder={placeholder} value={value} onChange={onChange} min={min} />;
};
