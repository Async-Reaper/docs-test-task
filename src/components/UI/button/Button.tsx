import React, { FC } from "react";
import { IButton } from "./Button.types";
import cl from "./Button.module.css";

/**
 * Component for showing button.
 *
 * @component
 * @example
 * const age = 21
 * return (
 *   <Button type="click" onClick={() => console.log(age)}>Click me</Button>
 * )
 */

const Button: FC<IButton> = ({ type, children, onClick, disabled }) => {
  return (
    <button disabled={disabled} className={cl.Button} type={type} onClick={onClick}>
      {children}
    </button>
  );
};

export default Button;
