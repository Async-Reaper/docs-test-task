import { ReactNode } from "react";

export interface IButton {
  type?: "submit" | "button" | "reset" | undefined;
  children: ReactNode;
  onClick?: () => void;
  disabled?: boolean;
}
