import { ITodo } from "../../../models/ITodo";

export interface IChangeTodoForm {
  todo: ITodo;
  setVisible: (arg: boolean) => void;
}
