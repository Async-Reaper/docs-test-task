import dayjs from "dayjs";
import React, { useState, useCallback, FC, useEffect } from "react";
import { ITodo } from "../../../models/ITodo";
import { changeTodo } from "../../../services/api/changeTodo";
import { addZero } from "../../../utils/addZero";
import Button from "../../UI/button/Button";
import Error from "../../UI/error/Error";
import { Input } from "../../UI/input/Input";
import cl from "./ChangeTodoForm.module.css";
import { IChangeTodoForm } from "./ChangeTodoForm.types";

/**
 * Component for showing show change todo.
 *
 * @component
 * @example
 * return (
 *   <ChangeTodoForm />
 * )
 */

const ChangeTodoForm: FC<IChangeTodoForm> = ({ todo, setVisible }) => {
  const [title, setTitle] = useState<string>(todo.title);
  const [description, setDescription] = useState<string>(todo.description);
  const [file, setFile] = useState<any>(null);
  const [finishDate, setDate] = useState<string>(
    `${dayjs().year()}-${addZero(dayjs().month() + 1)}-${addZero(dayjs().date())} ${addZero(dayjs().hour() + 1)}:${addZero(dayjs().minute())}`
  );
  const [error, setError] = useState<boolean>(false);

  const dataTask: ITodo = {
    id: Date.now(),
    title,
    description,
    finishDate,
    todoStatus: todo.todoStatus,
    todoId: todo.todoId,
  };

  const filesData: FormData = new FormData();
  filesData.append("file", file);

  const onChangeFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFile(e.target.files);
  };

  const postData = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      if (title !== "" && description !== "") {
        changeTodo(dataTask, file);
        setVisible(false);
        setError(false);
      } else {
        setError(true);
      }
    },
    [dataTask, error]
  );

  return (
    <form className={cl.Form} onSubmit={(e) => postData(e)} encType="multipart/form-data">
      {error && <Error>Все поля должны быть заполнены!</Error>}
      <label htmlFor="title">Заголовок</label>
      <Input id="title" type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
      <label htmlFor="description">Описание</label>
      <Input id="description" type="text" value={description} onChange={(e) => setDescription(e.target.value)} />
      <label htmlFor="date">Дата завершения</label>
      <Input id="date" type="datetime-local" value={finishDate} onChange={(e) => setDate((prev) => e.target.value)} min={finishDate} />
      <input type="file" multiple onChange={(e) => onChangeFile(e)} />
      <Button type="submit">Редактировать</Button>
    </form>
  );
};

export default ChangeTodoForm;
