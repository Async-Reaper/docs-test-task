import React, { useCallback, useState, FC } from "react";
import dayjs from "dayjs";
import { ITodo } from "../../../models/ITodo";
import { fetchTodo } from "../../../services/api/fetchTodo";
import cl from "./CreateTodoForm.module.css";
import { Input } from "../../UI/input/Input";
import Button from "../../UI/button/Button";
import { ICreateTodoForm } from "./CreateTodoForm.types";
import { addZero } from "../../../utils/addZero";
import Error from "../../UI/error/Error";

/**
 * Component for showing show change todo.
 *
 * @component
 * @example
 * const [visible, setVisible] = useState<boolean>(false)
 * return (
 *   <ModalWindow visible={visible} setVisible={setVisible}>
 *    <CreateTodoForm setVisible={setVisible} todo={todo}/>
 *  </ModalWindow>
 * )
 */

const CreateTodoForm: FC<ICreateTodoForm> = ({ setVisible }) => {
  const [title, setTitle] = useState<string>("");
  const [description, setDescription] = useState<string>("");
  const [finishDate, setDate] = useState<string>(
    `${dayjs().year()}-${addZero(dayjs().month() + 1)}-${addZero(dayjs().date())} ${addZero(dayjs().hour() + 1)}:${addZero(dayjs().minute())}`
  );
  const [todoStatus, setTodoStatus] = useState<boolean>(false);
  const [file, setFile] = useState<any>(null);
  const [error, setError] = useState<boolean>(false);

  const dataTask: ITodo = {
    id: Date.now(),
    title,
    description,
    finishDate,
    todoStatus,
  };

  const filesData: FormData = new FormData();
  filesData.append("file", file);

  const onChangeFile = (e: React.ChangeEvent<HTMLInputElement>) => {
    setFile(e.target.files);
  };

  const postData = useCallback(
    (e: React.FormEvent<HTMLFormElement>) => {
      e.preventDefault();
      if (title !== "" && description !== "") {
        fetchTodo(dataTask, file);
        setVisible(false);
        setError(false);
      } else {
        setError(true);
      }
    },
    [dataTask, error]
  );

  return (
    <form className={cl.Form} onSubmit={(e) => postData(e)} encType="multipart/form-data">
      {error && <Error>Все поля должны быть заполнены!</Error>}
      <label htmlFor="title">Заголовок</label>
      <Input id="title" type="text" value={title} onChange={(e) => setTitle(e.target.value)} />
      <label htmlFor="description">Описание</label>
      <Input id="description" type="text" value={description} onChange={(e) => setDescription(e.target.value)} />
      <label htmlFor="date">Дата завершения</label>
      <Input id="date" type="datetime-local" value={finishDate} onChange={(e) => setDate((prev) => e.target.value)} min={finishDate} />
      <input type="file" onChange={(e) => onChangeFile(e)} accept={".svg, .png, .jpg"} />
      <Button type="submit">Создать</Button>
    </form>
  );
};

export default CreateTodoForm;
