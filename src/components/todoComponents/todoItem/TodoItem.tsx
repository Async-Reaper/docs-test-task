import dayjs from "dayjs";
import { getDownloadURL, getStorage, listAll, ref } from "firebase/storage";
import { FC, useCallback, useEffect, useState } from "react";
import { compliteTodo } from "../../../services/api/compliteTodo";
import { deleteTodo } from "../../../services/api/deleteTodo";
import { addZero } from "../../../utils/addZero";
import Button from "../../UI/button/Button";
import ModalWindow from "../../UI/modalWindow/ModalWindow";
import ChangeTodoForm from "../changeTodo/ChangeTodoForm";
import cl from "./TodoItem.module.css";
import { ITodoItem } from "./TodoItem.types";

/**
 * Component for showing all todo.
 *
 * @component
 * @example
 * const index = [1]
 * const todos = [
    {
      id: 1,
      title: 'Title',
      description: 'Description',
      finishDate: '2022-11-20T23:00',
      todoId: 1,
      linkFiles: 'https://link.com',
    }
  ]
 * return (
 * {todos.map(todo => <TodoItem key={index} todo={todo}/>)}
 * )
 */

const TodoItem: FC<ITodoItem> = ({ todo }) => {
  const [visible, setVisible] = useState<boolean>(false);
  const [fileLinks, setFileLinks] = useState<any>();
  const [currentDate, setCurrentDate] = useState<string>(
    `${dayjs().year()}-${addZero(dayjs().month() + 1)}-${addZero(dayjs().date())} ${addZero(dayjs().hour())}:${addZero(dayjs().minute())}`
  );
  const [statusTodo, setStatusTodo] = useState<boolean>(false);
  const storage = getStorage();
  const listRef = ref(storage, `images/${todo.id}`);

  useEffect(() => {
    currentDate > todo.finishDate.replace(/T/i, " ") ? setStatusTodo(true) : setStatusTodo(false);

    listAll(listRef).then((res) => {
      res.items.forEach((itemRef) => {
        getDownloadURL(ref(storage, itemRef.fullPath)).then((url) => {
          const xhr = new XMLHttpRequest();
          xhr.responseType = "blob";
          xhr.onload = (event) => {
            const blob = xhr.response;
          };
          xhr.open("GET", url);
          xhr.send();

          setFileLinks(url);
        });
      });
    });
  }, [currentDate, todo.finishDate, todo]);

  return (
    <div className={cl.TodoItem}>
      {statusTodo ? <h1>Срок задачи истек</h1> : todo.todoStatus && <h1>Задача завершена</h1>}
      <p>Заголовок: {todo.title}</p>
      <p>Описание: {todo.description}</p>
      <p>Дата завершения: {todo.finishDate.replace(/T/i, " ")}</p>
      {fileLinks && (
        <div className={cl.ImageWrapper}>
          <a href={fileLinks}>Ссылка на картинку</a>
        </div>
      )}
      <Button onClick={() => compliteTodo(todo.todoId)}>Завершить задачу</Button>
      <Button disabled={todo.todoStatus} onClick={() => setVisible(true)}>
        Редактировать
      </Button>
      <Button onClick={() => deleteTodo(todo.todoId, todo.id)}>Удалить</Button>
      <ModalWindow visible={visible} setVisible={setVisible}>
        <ChangeTodoForm setVisible={setVisible} todo={todo} />
      </ModalWindow>
    </div>
  );
};

export default TodoItem;
