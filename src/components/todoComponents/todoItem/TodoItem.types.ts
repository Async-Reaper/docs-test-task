import { IGetTodo } from "../../../models/IGetTodo";

export interface ITodoItem {
  todo: IGetTodo;
}
