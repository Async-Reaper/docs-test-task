import { FC, useEffect, useState } from "react";
import { IGetTodo } from "../../../models/IGetTodo";
import { getAllTodo } from "../../../services/api/getAllTodo";
import TodoItem from "../todoItem/TodoItem";

/**
 * Component for showing all todo.
 *
 * @component
 * @example
 * return (
 *   <TodoList />
 * )
 */

const TodoList: FC = () => {
  const [allTodo, setAllTodo] = useState<IGetTodo[]>();

  useEffect(() => {
    const result = async () => {
      const response: any = await getAllTodo();
      const res = response;
      setAllTodo(res);
    };
    result();
  }, [allTodo]);

  return (
    <div>
      {allTodo?.map((todo) => (
        <TodoItem key={todo.id} todo={todo} />
      ))}
    </div>
  );
};

export default TodoList;
