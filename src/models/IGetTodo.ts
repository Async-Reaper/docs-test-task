export interface IGetTodo {
  id: number;
  title: string;
  description: string;
  finishDate: string;
  todoStatus: boolean;
  file?: any;
  todoId: number;
  linkFiles: any[];
}
