export interface ITodo {
  id: number;
  title: string;
  description: string;
  finishDate: string;
  todoStatus: boolean;
  file?: any;
  todoId?: number;
}
