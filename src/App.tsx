import { useState } from "react";
import "./App.css";
import TodoForm from "./components/todoComponents/createTodo/CreateTodoForm";
import TodoList from "./components/todoComponents/todoList/TodoList";
import Button from "./components/UI/button/Button";
import ModalWindow from "./components/UI/modalWindow/ModalWindow";

function App() {
  const [visible, setVisible] = useState<boolean>(false);
  return (
    <div className="App">
      <Button onClick={() => setVisible(true)}>Создать задачу</Button>
      <TodoList />
      <ModalWindow visible={visible} setVisible={setVisible}>
        <TodoForm setVisible={setVisible} />
      </ModalWindow>
    </div>
  );
}

export default App;
