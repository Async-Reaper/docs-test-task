import { ITodo } from "../../models/ITodo";
import { doc, Firestore, updateDoc } from "firebase/firestore";
import { db } from "../../firebase";
import { getStorage, ref, uploadBytesResumable } from "firebase/storage";

/**
 * Function for change todo.
 * @param {ITodo} data - The data for change todo.
 * @param {any} files - The files (if they are) for change todo.
 */

export const changeTodo = async (data: ITodo, files: any) => {
  try {
    /**
     * @constant {DocumentReference<DocumentData>} todoRef - Document current todo
     */
    const todoRef = doc(db, "tasks", String(data.todoId));
    await updateDoc(todoRef, {
      title: data.title,
      description: data.description,
      finishDate: data.finishDate,
    });

    /**
     * @constant storage - Storage with files.
     */
    const storage = getStorage();
    for (const i of files) {
      const storageRef = ref(storage, `images/${data.id}/` + i.name);
      const upload = uploadBytesResumable(storageRef, i);
    }
  } catch (error) {}
};
