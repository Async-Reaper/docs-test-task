import { deleteDoc, doc } from "firebase/firestore";
import { deleteObject, getStorage, listAll, ref } from "firebase/storage";
import { db } from "../../firebase";

/**
 * Function for delete todo.
 * @param {number} id - Id number for delete need todo.
 * @param {number} imageId - Id for delete folder with images current todo.
 */

export const deleteTodo = async (id: number, imageId: number) => {
  try {
    const storage = getStorage();
    const listRef = ref(storage, `images/${imageId}`);

    await deleteDoc(doc(db, "tasks", String(id)));
    await listAll(listRef).then((res) => {
      res.items.forEach((itemRef) => {
        const desertRef = ref(storage, itemRef.fullPath);
        deleteObject(desertRef);
      });
    });
  } catch (error) {
    console.log(error);
  }
};
