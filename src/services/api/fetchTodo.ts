import { doc, setDoc } from "firebase/firestore";
import { getStorage, ref, uploadBytesResumable } from "firebase/storage";
import { db } from "../../firebase";
import { ITodo } from "../../models/ITodo";

/**
 * Function for change todo.
 * @param {ITodo} data - The data for create todo.
 * @param {any} files - The files (if they are) for create todo.
 */

export const fetchTodo = async (data: ITodo, files: any) => {
  try {
    await setDoc(doc(db, "tasks", String(Date.now())), data);
    const storage = getStorage();

    for (let i of files) {
      const storageRef = ref(storage, `images/${data.id}/` + i.name);
      const upload = uploadBytesResumable(storageRef, i);
    }
  } catch (error) {
    console.log(error);
  }
};
