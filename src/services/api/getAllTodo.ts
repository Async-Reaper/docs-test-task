import { collection, getDocs } from "firebase/firestore";
import { getDownloadURL, getStorage, listAll, ListResult, ref } from "firebase/storage";
import { db } from "../../firebase";

/**
 * Function for get all todo.
 */

export const getAllTodo = async () => {
  try {
    /**
     * @constant {any[]} arr - Arr with all todo.
     */
    const arr: any[] = [];
    const querySnapshot = await getDocs(collection(db, "tasks"));

    querySnapshot.forEach((doc) => {
      const obj = doc.data();
      obj.todoId = doc.id;

      arr.push(obj);
    });
    // console.log(arr);
    return arr;
  } catch (error) {
    console.log(error);
  }
};
