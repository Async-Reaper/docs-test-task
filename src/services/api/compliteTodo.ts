import { doc, updateDoc } from "firebase/firestore";
import { db } from "../../firebase";

export const compliteTodo = async (id: number) => {
  try {
    const todoRef = doc(db, "tasks", String(id));
    await updateDoc(todoRef, {
      todoStatus: true,
    });
  } catch (error) {
    console.log(error);
  }
};
