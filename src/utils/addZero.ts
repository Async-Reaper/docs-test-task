/**
 * Function for add zero in the date elements.
 * @param {any} num - Number for add zero.
 */

export const addZero = (num: any) => {
  if (num < 10) {
    return "0" + num;
  } else {
    return num;
  }
};
