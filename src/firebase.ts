import { FirebaseApp, FirebaseOptions, initializeApp } from "firebase/app";
import { Firestore, getFirestore } from "firebase/firestore";

const firebaseConfig: FirebaseOptions = {
  apiKey: "AIzaSyCTch4CCDiaqAa73KaCxjZupwx0MrAJobc",
  authDomain: "test-quetions.firebaseapp.com",
  databaseURL: "https://test-quetions-default-rtdb.firebaseio.com",
  projectId: "test-quetions",
  storageBucket: "test-quetions.appspot.com",
  messagingSenderId: "76138401444",
  appId: "1:76138401444:web:072d5ed7e025074ad72031",
  measurementId: "G-0Z3G6CV3Y3",
};

export const app: FirebaseApp = initializeApp(firebaseConfig);
export const db: Firestore = getFirestore(app);
